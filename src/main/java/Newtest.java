import java.util.*;

public class Newtest {

    static int[] oddNumbers(int l, int r) {
        int array[] = null;

        if (l % 2 == 0 && r % 2 == 0)
            array = new int[(r - l) / 2];
        else if (l % 2 != 0 && r % 2 != 0)
            array = new int[((r - l) / 2) + 1];
        else if (l % 2 == 0 || r % 2 == 0)
            array = new int[((r - l) / 2) + 1];

        int k = 0;
        while (l < r + 1) {
            if (l % 2 != 0)
                array[k++] = l;
            l++;
        }

        return array;
    }

    public static void main(String[] args) {
        System.out.println(getMinimumUniqueSum("17 24"));
    }

    static int[] getMinimumUniqueSum(String[] arr) {

        if (arr == null)
            return new int[]{};

        List<Integer> list = new LinkedList<>();

        for (String eachString : arr) {
            list.add(getMinimumUniqueSum(eachString));
        }

        int array[] = new int[list.size()];
        int idx = 0;
        for (int e : list) {
            array[idx++] = e;
        }

        return array;
    }

    static int getMinimumUniqueSum(String arr) {
        if (arr == null)
            return 0;

        String[] m = arr.split(" ");
        int start = Integer.parseInt(m[0]);
        int end = Integer.parseInt(m[1]);
        int squares = 0;
        int i = 1;

        while (true) {
            if (i * i >= start && i * i <= end) {
                squares++;
            }
            if (i > Math.sqrt(end))
                break;
            else
                i++;
        }

        return squares;
    }


    static String[] doesCircleExist(String[] commands) {
        List<String> list = new LinkedList<>();

        for (String eachCommand : commands) {
            list.add(doesCircleExist(eachCommand));
        }

        return list.toArray(new String[list.size()]);
    }


    static String doesCircleExist(String command) {
        int x = 0, y = 0;

        Set<Point> set = new HashSet<>();
        Stack<Character> stack = new Stack<>();

        for (char each : command.toCharArray()) {
            stack.push(each);
        }

        while (true) {
            if (stack.peek() == 'R' || stack.peek() == 'L' && stack.size() == 1) {
                stack.pop();
                return "YES";
            }

            if (stack.peek() == 'G' && stack.size() == 1) {
                stack.pop();
                return "NO";
            }

            if (stack.isEmpty())
                break;
        }


        return "NO";
    }

    static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }


    }


}
