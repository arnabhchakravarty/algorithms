
public class MergeSort {


    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        int[] sorted = mergeSort(new int[]{454,456,45,6456,46462,344,2,42,66,465,464,747,657,10,3,6,7,8,23,45,67,34,23,45,555,456,56,34,43,4,6,456,456,4564,646,46,464,64,646,46,4,7,5,85,697,97,9,64,64,54,53,53,34,3,35,43,47,58,69,69,6,46,25,235,23,47,58,458,537,437,3});
        System.out.println("Sorted in : " + (System.currentTimeMillis() - startTime));
        for(int s : sorted)
            System.out.print(s+ ",");
        System.out.println(System.currentTimeMillis());
    }


    public static int[] mergeSort(int[] unsortedArray) {

        // breakup the unsorted array into 2
        int[][] twoArrays = splitInHalf(unsortedArray);

        // recursively call mergeSort until the array size is not equal to 1
        int[] unsortedArrayOne = twoArrays[0];
        int[] unsortedArrayTwo = twoArrays[1];

        int[] sortedArrayOne;
        if(unsortedArrayOne.length < 3)
            sortedArrayOne = sort(unsortedArrayOne);
        else
            sortedArrayOne = mergeSort(unsortedArrayOne);

        int[] sortedArrayTwo;
        if(unsortedArrayTwo.length < 3)
            sortedArrayTwo = sort(unsortedArrayTwo);
        else
            sortedArrayTwo = mergeSort(unsortedArrayTwo);

        // merge 2 sorted arrays
        return merge2SortedArrays(sortedArrayOne, sortedArrayTwo);

    }

    public static int[] sort(int[] array) {
        if(array.length == 1)
            return array;
        else {
            if(array[0] > array[1]){
                int t = array[0];
                array[0] = array[1];
                array[1] = t;
            } else if(array[0] == array[1]) {
                return array;
            }
        }

        return array;
    }

    ///[1,2,3]
    public static int[][] splitInHalf(int[] arrayToSplit){
        int[][] N = new int[2][];
        int half = arrayToSplit.length/2; // 1

        N[0] = new int[half];

        int remaining = arrayToSplit.length - half;
        N[1] = new int[remaining];

        System.arraycopy(arrayToSplit, 0, N[0], 0, half); //0, 1
        System.arraycopy(arrayToSplit, half, N[1], 0, remaining); // 1, 2

        return N;
    }

    //[1,2,3] [4,5,6]
    public static int[] merge2SortedArrays(int[] firstArray, int[] secondArray) {
        int[] joined = new int[firstArray.length + secondArray.length]; // 6

        int i = 0, j = 0, k = 0;
        for(; i < firstArray.length && j < secondArray.length; ){ // i < 3 & j < 3
            if(firstArray[i] < secondArray[j]){ // 0,0
                joined[k++] = firstArray[i++]; //
            } else if(firstArray[i] > secondArray[j]) {
                joined[k++] = secondArray[j++];
            } else {
                joined[k++] = firstArray[i++];
                joined[k++] = secondArray[j++];
            }
        }

        if(i == firstArray.length && j < secondArray.length) {
            while(j < secondArray.length)
                joined[k++] = secondArray[j++];
        }

        if(j == secondArray.length && i < firstArray.length) {
            while(i< firstArray.length)
                joined[k++] = firstArray[i++];
        }

        return joined;
    }


}