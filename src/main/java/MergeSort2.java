public class MergeSort2 {

    private int[] array;

    public MergeSort2(int[] array) {
        this.array = array;
    }

    private boolean sort() {
        if(array.length == 2)
            array = join(array);

        int[][] newArray = split(array);

        return true;
    }


    private int[] join() {
        System.out.println(split(array));
        return null;
    }

    private int[][] split(int[] array) {
        int N = array.length/2;

        int rightSize = array.length - N;

        int[] leftArray = new int[N];
        int[] rightArray = new int[rightSize];

        System.arraycopy(array, 0, leftArray, 0, N);
        if(leftArray.length > 1)
            split(leftArray);

        System.arraycopy(array, N, rightArray, 0, rightSize);
        if(rightArray.length > 1)
            split(rightArray);

        return new int[][]{leftArray, rightArray};
    }

    private static int[] join(int[] l) {
        if(l.length == 1)
            return l;

        if (l[0] > l[1]) {
            int t = l[1];
            l[1] = l[0];
            l[0] = t;
            return l;
        } else
            return l;
    }

    public static void main(String[] args) {
        new MergeSort2(new int[]{1,3,4,8,119,32,90}).sort();
    }

}
