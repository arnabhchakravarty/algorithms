import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ReverseList {

    public static void main(String[] args) {

        List<Integer> listOfInteger = new ArrayList<>();
        IntStream.range(0, 10).forEach(listOfInteger::add);
        System.out.println(listOfInteger);

        List<Integer> newList = new LinkedList<>();
        for (int k = listOfInteger.size() - 1; k > -1; k--)
            newList.add(listOfInteger.remove(k));

        newList.replaceAll(o -> (o - 10));

        System.out.println(newList);
        System.out.println(1 << 64);
        System.out.println(Math.pow(2, 13));

        Map<Integer, Integer> map = new HashMap<>();
        Stream<Map<Integer, Integer>> newmap = newList.stream().map(integer -> {
            map.put(integer, integer - 10);
            return map;
        });

        System.out.println(newmap);

        NoHashCode nohashCode1 = new NoHashCode("Try");
        NoHashCode nohashCode2 = new NoHashCode("Try");

        System.out.println(nohashCode1.hashCode());
        System.out.println(nohashCode2.hashCode());

    }


    static class NoHashCode {
        String data;

        public NoHashCode(String data) {
            this.data = data;
        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }
    }




}
