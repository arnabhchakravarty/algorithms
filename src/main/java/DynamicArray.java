//import java.io.BufferedWriter;
//import java.io.FileDescriptor;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Scanner;
//
//public class DynamicArray {
//
//    private static final Scanner scanner = new Scanner(System.in);
//
//    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FileDescriptor.out));
//
//        String[] nq = scanner.nextLine().split(" ");
//
//        int n = Integer.parseInt(nq[0].trim());
//
//        int q = Integer.parseInt(nq[1].trim());
//
//        int[][] queries = new int[q][3];
//
//        for (int queriesRowItr = 0; queriesRowItr < q; queriesRowItr++) {
//            String[] queriesRowItems = scanner.nextLine().split(" ");
//
//            for (int queriesColumnItr = 0; queriesColumnItr < 3; queriesColumnItr++) {
//                int queriesItem = Integer.parseInt(queriesRowItems[queriesColumnItr].trim());
//                queries[queriesRowItr][queriesColumnItr] = queriesItem;
//            }
//        }
//
//        int[] result = dynamicArray(n, queries);
//
//        for (int resultItr = 0; resultItr < result.length; resultItr++) {
//            bufferedWriter.write(String.valueOf(result[resultItr]));
//
//            if (resultItr != result.length - 1) {
//                bufferedWriter.write("\n");
//            }
//        }
//
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();
//    }
//
//    static int[] dynamicArray(int n, int[][] queries) {
//        int lastAnswer = 0;
//
//        List<Integer> lastValueList = new LinkedList<>();
//
//        // create n arrays/seq
//        List<List<Integer>> seq = new LinkedList<>();
//        for(int j = 0; j < n; j++){
//            seq.add(j, new LinkedList<>());
//        }
//
//        // iterate over the queries
//        for (int i = 0; i < queries.length; i++) {
//            // find sequence
//            int sequenceUsingXOR = findSequenceUsingXOR(queries[i][1], lastAnswer, n);
//            List<Integer> requiredSequence = seq.get(sequenceUsingXOR);
//            int queryType = queries[i][0];
//            if ((queryType % 2) != 0) {
//                requiredSequence.add(queries[i][2]);
//            } else {
//                lastAnswer = requiredSequence.get((queries[i][2]) % requiredSequence.size());
//                lastValueList.add(lastAnswer);
//            }
//        }
//        int[] lastValueArray = new int[lastValueList.size()];
//        int index = 0;
//        for (Integer eachValue : lastValueList) {
//            lastValueArray[index++] = eachValue.intValue();
//        }
//        return lastValueArray;
//    }
//
//    static int findSequenceUsingXOR(int leftOperand, int rightOperand, int numberOfSequences) {
//        return (leftOperand ^ rightOperand) % numberOfSequences;
//    }
//
//}
