import java.io.BufferedWriter;
import java.io.FileDescriptor;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DynamicArray2Optimized {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FileDescriptor.out));

        String[] nq = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nq[0].trim());

        int q = Integer.parseInt(nq[1].trim());

        int[][] queries = new int[q][3];

        for (int queriesRowItr = 0; queriesRowItr < q; queriesRowItr++) {
            String[] queriesRowItems = scanner.nextLine().split(" ");

            for (int queriesColumnItr = 0; queriesColumnItr < 3; queriesColumnItr++) {
                int queriesItem = Integer.parseInt(queriesRowItems[queriesColumnItr].trim());
                queries[queriesRowItr][queriesColumnItr] = queriesItem;
            }
        }

        int[] result = dynamicArray(n, queries);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bufferedWriter.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();
    }

    static int[] dynamicArray(int n, int[][] queries) {
        int lastAnswer = 0;

        int[] lastAnswerList = null;

        // create n arrays/seq
        int[][] seq = new int[n][];

        // iterate over the queries
        for (int i = 0; i < queries.length; i++) {
            // find sequence
            int sequenceUsingXOR = findSequenceUsingXOR(queries[i][1], lastAnswer, n);
            int[] requiredSequence = seq[sequenceUsingXOR];
            int queryType = queries[i][0];
            if ((queryType % 2) != 0) {
                int[] newArray;
                if(requiredSequence == null) {
                    newArray = new int[1];
                    newArray[0] = queries[i][2];
                } else {
                    newArray = new int[requiredSequence.length+1];
                    System.arraycopy(requiredSequence, 0, newArray, 0, requiredSequence.length);
                    newArray[requiredSequence.length] = queries[i][2];
                }
                seq[sequenceUsingXOR] = newArray;
            } else {
                lastAnswer = requiredSequence[(queries[i][2]) % requiredSequence.length];
                if(lastAnswerList == null) {
                    lastAnswerList = new int[1];
                    lastAnswerList[0] = lastAnswer;
                } else {
                    int newArray[] = new int[lastAnswerList.length + 1];
                    System.arraycopy(lastAnswerList, 0, newArray, 0, lastAnswerList.length);
                    newArray[lastAnswerList.length] = lastAnswer;
                    lastAnswerList = newArray;
                }
            }
        }

        return lastAnswerList;
    }

    static int findSequenceUsingXOR(int leftOperand, int rightOperand, int numberOfSequences) {
        return (leftOperand ^ rightOperand) % numberOfSequences;
        // (leftOperand | rightOperand) & ~(leftOperand & rightOperand)
    }

}
